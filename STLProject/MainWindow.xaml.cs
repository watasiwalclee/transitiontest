﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using HiAPI.Common;
using HiAPI.Disp;
using HiAPI.Disp.Comp;
using HiAPI.Geom;
using OpenGL;

namespace STLProject
{
	/// <summary>
	/// MainWindow.xaml 的互動邏輯
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			//Form form = FormUtil.Show(new Displayer(new CoordinateDraw("Hello HiAPI")));

		}

		[STAThread]

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Mat4d moveMatrix = new Mat4d(1, 0, 0, 0,
										 0, 1, 0, 0,
										 0, 0, 1, 0,
										 1, 1, 1, 1);

			Mat4d position = new Mat4d(new Vec3d(0,0,0));
			Mat4d res = moveMatrix * position;


			Tri3d tri3D = new Tri3d(new Vec3d(0,0,0), new Vec3d(1, 0, 0), new Vec3d(0, 1, 0));

			//Form form = FormUtil.Show(new Displayer(box3D, new CoordinateDraw("ori"), res, new CoordinateDraw()));
			DisplayerForm.Call("test", tri3D.ToDraw(), new CoordinateDraw { Tag="ori" } ,new CoordinateDraw());
		}
		private void TrasitionEvent(object sender, RoutedEventArgs e)
		{
			Transition transition = new Transition();
			try
			{

				Tri3d tri3D = new Tri3d(new Vec3d(0, 0, 0), new Vec3d(1, 0, 0), new Vec3d(0, 1, 0));
				double MovtivationX = Convert.ToDouble(PositionX.Text);
				double MovtivationY = Convert.ToDouble(PositionY.Text);
				double MovtivationZ = Convert.ToDouble(PositionZ.Text);

				Mat4d OriginPosition = new Mat4d(new Vec3d(0, 0, 0));
				Mat4d MotivationResult = transition.StaticTranslate(MovtivationX, MovtivationY, MovtivationZ, OriginPosition);
				DisplayerForm.Call("Coordinate", new CoordinateDraw());
			}
			catch
			{
				System.Windows.Forms.MessageBox.Show("請輸入正確數值");
			}

		}

		private void RotationEvent(object sender, RoutedEventArgs e)
		{
			Transition transition = new Transition();
			try
			{

				Box3d box3D = new Box3d(0, 0, 0, 1, 1, 1);

				double RotationAngelX = Convert.ToDouble(AngelX.Text);
				double RotationAngelY = Convert.ToDouble(AngelY.Text);
				double RotationAngelZ = Convert.ToDouble(AngelZ.Text);
				Mat4d RotationResult = transition.StaticRotate(RotationAngelX, RotationAngelY, RotationAngelZ);

				DisplayerForm.Call("Rotation Test");
			}
			catch
			{
				System.Windows.Forms.MessageBox.Show("請輸入正確數值");
			}


		}

	}
}
