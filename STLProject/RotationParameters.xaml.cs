﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace STLProject
{
	public class GlobalRotationParameters
	{

	}
	/// <summary>
	/// RotationParameters.xaml 的互動邏輯
	/// </summary>
	public partial class RotationParameters : UserControl
	{
		public RotationParameters()
		{
			InitializeComponent();
		}


		private void TextBoxXAngel_TouchEnter(object sender, TouchEventArgs e)
		{
			RotationParameters rotationParameters = new RotationParameters();
			try
			{
				double xangel = Convert.ToDouble(rotationParameters.TextBoxXAngel.Text);
				rotationParameters.SliderXAngel.Value = xangel;
			}
			catch
			{
				MessageBox.Show("請輸入旋轉角");
			}
		}

		private void SliderXAngel_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			RotationParameters rotationParameters = new RotationParameters();
			rotationParameters.TextBoxXAngel.Text = Convert.ToString(rotationParameters.SliderXAngel.Value);
		}
	}
}
