﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiAPI.Geom;

namespace STLProject
{
	public class Transition
	{
		public Mat4d StaticTranslate(double MovtivationXstep, double MovtivationYstep, double MovtivationZstep, Mat4d OriginPosition)
		{
			//Mat4d OriginPosition = new Mat4d(new Vec3d(0, 0, 0));
			Mat4d TransitionMatrix = new Mat4d(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				MovtivationXstep, MovtivationYstep, MovtivationZstep, 1
			);


			Mat4d MotivationResault = TransitionMatrix * OriginPosition;
			return MotivationResault;
		}

		public Mat4d StaticRotate(double AngelX, double AngelY, double AngelZ)
		{
			Mat4d Position = new Mat4d(new Vec3d(0, 0, 0));


			Mat4d RotationResult = Position;
			return RotationResult;
		}
	}
}
